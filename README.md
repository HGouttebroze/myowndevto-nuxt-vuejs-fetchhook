- title: \* is My Own Dev-to Application
- author:
  _ HuguesG (Follow Your Dreams, Dev Fullstack)
  _ GitLab App Repo [here:](https://gitlab.com/HGouttebroze/myowndevto-nuxt-vuejs-fetchhook)

# Here is My Own Dev-to Application, build on Nuxt.js, a Vue.js framework SSG/SSR

To share post tech, coding ways and all that I love about my coding passion, open-source learning and sharing.

Nuxt.js is coming from

## 1-Init-project on GitLab

- You can find the open-source repo [here](https://gitlab.com/HGouttebroze/myowndevto-nuxt-vuejs-fetchhook)

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

## CSS-Styles-Instalation & Configuration

# use pre-processor SCSS

- to use with Nuxt run:

`yarn add sass sass-loader@10 -D`

- use `@nuxtjs/style-resources` module that will help us to use our design tokens defined in SCSS variables in any Vue file without the necessity of using @import statements in each file.

`yarn add @nuxtjs/style-resources`

- Now tell Nuxt to use it by adding this code to `nuxt.config.js` file

```js
buildModules: ["@nuxtjs/style-resources"];
```

- Let’s define our design tokens as SCSS variables, \* put them in :
  `~/assets/styles/tokens.scss`
  and tell
  `@nuxtjs/style-resources`
  to load them by adding to nuxt.config.js

- nuxt.config.js

```js
styleResources: {
  scss: ["~/assets/styles/tokens.scss"];
}
```

- Our design tokens are now available through SCSS variables in every Vue component.

* install npm dependencies
  `yarn add vue-content-placeholders vue-observe-visibility`

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).

### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).
