# Angular

## Commands

- Structure: `ng new app-name` + package.json: `yarn init`
  `ng serve`

- créer 1 documentation
  - `yarn add --dev @compodoc/compodoc`
  - Définissez ensuite un npm "script" dans votre fichier package.json
    `"scripts": { "compodoc": "compodoc -p src/tsconfig.app.json"}`
  - lancez la ensuite dans votre terminal :
    `yarn compodoc`
    Vous obtiendrez ensuite la documentation générée dans le dossier documentation.
    Ouvrez la dans votre navigateur

## Service

- `ng g s`
- on envoit les fichier des services dans 1 dossier tel 'svices/book'
- on importe & déclare le service ds l composant ts de notre choix (le + souvent le app.module, pr avoir le service dispo en global)
  exemple, on l déclre ds un array des providers & on importe le sercice

```
import { BookService } from './services/book/book.service';

@NgModule({
 declarations: [AppComponent],
 imports: [BrowserModule, AppRoutingModule],
 providers: [BookService],
 bootstrap: [AppComponent],
})
export class AppModule {}
```

- puis on inject ce service ds le app.component.ts, on créé 1 constructer

## Composants

`yarn ng generate component book/book-preview`

## Angular Form Validation Example Tutorial!

- Angular: a platform to build applications with the web.
- Angular combines:
  - declarative templates,
  - dependency injection,
  - an end to end tooling,
  - and integrated best practices to solve development challenges.
- User actions such as:
  _ clicking a link, pushing a button
  _ entering text raise DOM events.
  Forms are the mainstay of business applications. You use forms to log in,
  submit a help request, place an order, book a flight, schedule a meeting,
  and perform countless other data-entry tasks. Improve overall data quality
  by validating user input for accuracy and completeness.

# Bootstrap install with npm

`npm i --save bootstrap`

# Méthodes, Notions & Directives

- 'string interpolation' & 'property binding':
  - 2 notions s essentielles
  - des moyens pr faire passer les infos depuis le code (ds la ârtie ts)
    rs l'affochage, soit le HTML
  - vLpermet la transmission d'informatioss
- 'string interpolation': \* permet d afficher 1 valeur a l'écran
  HTML <h5 class="card-title">{{book.title}} - {{book.status}}</h5>
  <h6 class="card-subtitle mb-2 text-muted">{{book.author}}</h6>
  <p class="card-text">{{book.description}}</p>
  TS

* DATA BINDING :

  - permet communication du HTML vers TS

          communication: HTML ---> TS

- property binding:
