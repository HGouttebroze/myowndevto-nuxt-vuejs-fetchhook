Le développement piloté par les tests est un principe de développement logiciel qui garantit que votre code fonctionne exactement comme vous le souhaitez, c'est un principe qui devrait être suivi par chaque développement bien qu'il y ait beaucoup de gens qui doivent encore suivre le principe, suivez le développement piloté par les tests, puis vous suivrez le développement ultérieur du débogage
Pourquoi devriez-vous suivre le principe de développement piloté par les tests
Cela facilite le débogage : imaginez simplement qu'il s'agit d'un bogue ou d'un problème dans votre base de code avec une approche de développement pilotée par les tests, vos cas de test échoueront certainement et vous saurez exactement où chercher, quoi rechercher et quoi modifier. cela facilite le développement.
Il fonctionne comme vous le vouliez : Test Driven Development vous aide et garantit que votre code fonctionne exactement comme vous le souhaitez.
Types de développement piloté par les tests
Test de l'unité
Le test unitaire est un type de test de développement logiciel où les composants individuels/unitaires d'un logiciel sont testés. Le processus est effectué pendant le développement d'une application, pour assurer et vérifier l'exactitude d'une unité/composant de code.
Test d'intégration
Le test d'intégration est le processus de test de la collection/groupe d'unité/composant. Le test d'intégration est effectué à l'issue des tests unitaires
Test d'acceptation/E2E (de bout en bout)
Les tests de bout en bout sont une méthodologie utilisée pour tester si le flux d'une application fonctionne comme prévu du début à la fin. Le but des tests de bout en bout est d'identifier les dépendances du système et de s'assurer que les bonnes informations sont transmises entre les divers composants et systèmes du système.
Cycle de développement piloté par les tests
Il y a 3 cycles derrière le développement piloté par les tests Red, Green etRefactor tout comme il y a 3 cycles derrière le contrôle des feux rouges, jaunes et verts.

Rouge : écrivez un test et il échoue
Vert : écrivez un test pour une fonctionnalité et assurez-vous qu'elle passe le test
Refactor : Optimiser le test passé précédent et s'assurer que tous les cas de test réussissent.
Qu'est-ce que Jest ?
Jest est un charmant framework de test JavaScript axé sur la simplicité.
Il fonctionne avec des projets utilisant : Babel , TypeScript , Node , React , Angular , Vue et plus encore !
Comment installer jest dans votre projet Javascript
en utilisant du fil, vous installez jest avec le code ci-dessous
fil ajouter --dev jest
en utilisant npm, vous pouvez installer jest avec le code ci-dessous
npm install --save-dev jest
Mot-clé TDD que vous devriez connaître dans Jest
expect : lorsque vous écrivez des tests, vous devez souvent vérifier que les valeurs remplissent certaines conditions. expectvous donne accès à un certain nombre de "matchers" qui vous permettent de valider différentes choses. expect accepter une valeur (une valeur que vous souhaitez vérifier par rapport à ce que vous attendez).
Par exemple
attendre(somme(1,2)).toBe(3);
attendre(somme(1,2)).toBeA(nombre);
attendre(somme(1,2).toEqual(3);
la fonction expect a un matcher que vous pouvez utiliser et enchaîner avec en plaisantant. tels que .toBe(), toBeA , toEqual, toContainEqualetc. consultez la documentation officielle pour plus de correspondances attendues.
test/it :test ou itest une fonction qui exécute le test, elle vous permet de tester votre fonction unitaire par exemple. Disons qu'il existe une fonction appelée sumqui accepte deux paramètres et renvoie l'addition des paramètres.
test('Somme de deux nombres', () => {
expect(Sum(5,9)).toBe(14); }
);
vérifier la documentation officielle pour matcher
Comment démarrer avec Jest en utilisant Javascript
initialiser le projet javascript avec npm en utilisant votre terminal
npm init --y
installez jes t dans votre projet à l'aide du terminal
npm install --save-dev jest
NB : Vous pouvez créer votre fichier de test avec une .test.jsextension qui permettra à jest de localiser votre fichier et d'exécuter vos cas de test. Pour organiser et structurer votre projet, vous pouvez créer un dossier avec le nom **test** dans votre répertoire de projet qui abritera tous les fichiers de cas de test.
Exemple
Supposons que l'on vous confie la tâche d'écrire une fonction qui accepte deux valeurs en tant que paramètre et additionne les deux valeurs. la question est maintenant de savoir ce que vous devez tester. Il y a beaucoup de choses à tester car tout ce que vous avez à faire est de réfléchir au type de sortie/résultat que vous souhaitez renvoyer à l'utilisateur. Vous trouverez ci-dessous ce que vous pouvez tester.
Testez si la valeur des paramètres est de type de données nombre, s'il ne s'agit pas d'un nombre, le test devrait échouer.
Testez si la somme du paramètre aet du paramètre best logiquement correcte, sinon le test devrait échouer.
NB: Si vous voulez que votre paramètre accepte le tableau et additionne également le tableau. vous pouvez écrire un scénario de test qui vérifie si l'utilisateur fournit un tableau en tant que paramètre et vérifie s'il renvoie une réponse correcte.
Solution
Il existe deux manières de TDD, soit vous écrivez vos cas de test avant de commencer réellement votre développement, soit vous écrivez les cas de test plus tard après le développement, mais la meilleure pratique est d'abord le cas de test, puis le développement. c'est l'approche que nous allons suivre maintenant.
Créons un dossier appelé **test**au niveau racine de notre projet, à l'intérieur du dossier créez un fichier appelé sum.test.js, tous nos cas de test pour la fonction somme seront dans le fichier.
NB: j'utiliserai commonJS, vous pouvez utiliser et configurer votre projet pour utiliser babel, s'il est déjà configuré avec babel donc tout va bien
//import somme
const { somme } = require("./somme");
describe('Ajout du test de fonctionnalité à deux nombres',()=>{
test("Ajouter 1 + 2 devrait être 3", () => {
attendre(somme(1, 2)).toBe(3);
attendre(somme(1,2)).toBeA(nombre);
});
test("il devrait échouer si chaîne en paramètre", () => {
expect(sum("Bonjour", "Monde"))
.toEqual(Error("Type de nombre attendu comme paramètre"));
});
});
Explication
Le premier cas de test vérifiera si le premier paramètre et la deuxième valeur du paramètre renvoient une réponse correcte. c'est-à-dire si 1+2 égal à 3.
test("Ajouter 1+2 devrait être 3", () => {
attendre(somme(1, 2)).toBe(3);
});
Le deuxième cas de test vérifiera si le premier paramètre ou la deuxième valeur de paramètre est une chaîne et renverra une erreur.
test("il devrait échouer si chaîne en paramètre", () => {
expect(sum("Bonjour", "Monde"))
.toEqual(Error("Type de nombre attendu comme paramètre"));
Maintenant que nos cas de test ont été écrits, nous devons seulement nous assurer que notre sum fonction réussit tous les cas de test. Laissez créer un fichier appelé sum.jsdans le dossier racine, le fichier contiendra une fonction qui accepte deux nombres et additionne les deux nombres
somme const = (a, b) => {
if (typeof a !== "nombre" || typeof b !== "nombre")
return Error("Type de nombre attendu comme paramètre");
retourner a + b;
} ;
module.exports = { somme };
NB: l'extrait ci-dessus vérifie d'abord le type de données du paramètre s'il ne s'agit pas d'un nombre, il devrait renvoyer une erreur avec le message Type de nombre attendu comme paramètre
La prochaine étape consiste à exécuter notre test, avant cela, nous devons configurer notre projet pour qu'il fonctionne avec jest, laisser aller directement au package.jsonfichier et ajouter “test”: “jest”à la section de script. notre script devrait ressembler au code ci-dessous.
"scripts": {
"test": "blague"
},
nous pouvons maintenant exécuter notre test avec npm testou la commande de test de fil, ci-dessous se trouve le résultat/sortie de notre test

Tous les cas de test réussis
D'accord, laissez-moi vous montrer l'un des cas de test que j'ai écrits pour la connexion, il existe de nombreuses façons de faire des cas de test, tout ce que vous avez à faire est de réfléchir à ce que vous voulez vérifier.
describe("Module de connexion pour les cas de test", () => {
it("devrait retourner l'utilisateur si le nom est valide", function(done) {
demande (application)
.post("/connexion")
.send({ nom d'utilisateur : "DAMMAK", mot de passe : "Adedamola" })
.end(function(err, res) {
expect(res.statusCode).toEqual(200);
expect(res.text).toEqual(
JSON.stringify({ nom d'utilisateur : "DAMMAK", mot de passe : "Adedamola" })
);
terminé();
});
});
it("échoue si les informations de connexion sont erronées", function(done) {
demande (application)
.post("/connexion")
.send({ nom d'utilisateur : "wrongUser", mot de passe : "wrongPass" })
.end(function(err, res) {
expect(res.statusCode).toEqual(400);
terminé();
});
});
it("échoue si des données de requête vides ont été envoyées", function(done) {
demande (application)
.post("/connexion")
.envoyer({})
.end(function(err, res) {
expect(res.statusCode).toEqual(400);
terminé();
});
});
});
