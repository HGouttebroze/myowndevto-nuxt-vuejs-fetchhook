# Install Storybook

Use the Storybook CLI to install it in a single command. Run this inside your existing project’s root directory:

## Add Storybook:

npx sb init
Copy
If you run into issues with the installation, check the Troubleshooting section below for guidance on how to solve it.

sb init is not made for empty projects
During its install process, Storybook will look into your project's dependencies and provide you with the best configuration available.

The command above will make the following changes to your local environment:

📦 Install the required dependencies.
🛠 Set up the necessary scripts to run and build Storybook.
🛠 Add the default Storybook configuration.
📝 Add some boilerplate stories to get you started.
Depending on your framework, first build your app and then check that everything worked by running:

NPM
YARN

## Starts Storybook in development mode

npm run storybook
Copy
It will start Storybook locally and output the address. Depending on your system configuration, it will automatically open the address in a new browser tab and you'll be greeted by a welcome screen.

Storybook welcome screen

There are some noteworthy items here:

A collection of useful links for more in depth configuration and customization options you have at your disposal.
A second set of links for you to expand your Storybook knowledge and get involved with the ever growing Storybook community.
A few example stories to get you started.
Troubleshooting
Now that you installed Storybook successfully, let’s take a look at a story that was written for us.

NEXT
What's a story?
Learn how to save component examples as stories
✍️ Edit on GitHub – PRs welcome!

### save

-     <h2>Hugues G., JavaScript FullStact developper, passion by coding application, opensource, sharing knowlede</h2>

- ```vue
  <template>
    <footer>
      <span>Developping in LOVE & PASSION by </span>
      <a href="https://nuxtjs.org" target="_blank">
        <nuxt-icon class="nuxt-icon" />
        FollowYourDreams's Hugues, Fullstack Dev,
      </a>
      <span>&</span>
      <a href="https://docs.dev.to/api" rel="nofollow noopener" target="_blank">
        <dev-to-icon />
      </a>
      <span>Build with</span>
      <a href="https://nuxtjs.org" target="_blank">
        <nuxt-icon class="nuxt-icon" />
      </a>
      <span>&</span>
      <a href="https://docs.dev.to/api" rel="nofollow noopener" target="_blank">
        <dev-to-icon />
      </a>
      <span
        >FollowYourDreams SOUTIEN
        <ul>
          <li>ANIMA</li>
          <li>LYDRA, FROGGIT & les Compagnons du DevOps</li>
          <li>Humanbooster, formateurs, encadrents & la session CDA17 !!!</li>
        </ul>
      </span>
    </footer>
  </template>
  ```

<script>
import DevToIcon from '@/assets/icons/dev-to.svg?inline'
import NuxtIcon from '@/assets/icons/nuxt.svg?inline'

export default {
  components: {
    DevToIcon,
    NuxtIcon
  }
}
</script>

<style lang="scss" scoped>
footer {
  padding: 2rem;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;

  span {
    display: inline-block;
    line-height: 1;
    text-transform: uppercase;
    letter-spacing: $-ls2;
    font-size: $text-xs;
    font-weight: $bold-body-font-weight;
  }
  a {
    svg {
      width: 3rem;
      height: 3rem;
      margin: 0 0.5rem;
    }
    .nuxt-icon {
      width: 2.5rem;
      height: 2.5rem;
      margin: 0 0.25rem;
    }
  }
}
</style>

```

```
