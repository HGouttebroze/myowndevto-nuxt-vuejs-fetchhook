const { CI_PAGES_URL } = process.env;
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname;

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  mode: "universal",
  head: {
    title: "FOLLOW YOUR DREAMS - Blog Post",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "FOLLOW YOUR DREAMS - Blog Post - DevTo - NuxtJS - ElasticSarch - Docker - GitLab",
        content:
          "Building a Blog Post on DevTo API (new fetch() hook) with NuxtJS, use nuxt-leaflet, ElasticSearch & deploy with Docker on  GitLab Pages",
      },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Inter:400,500,600&display=swap",
      },
    ],
  },

  css: [
    "~/assets/styles/reset.scss",
    "~/assets/styles/base.scss",
    "~/assets/styles/highlight.scss",
    "~/assets/styles/app.scss",
  ],
  styleResources: {
    scss: ["~/assets/styles/tokens.scss"],
  },
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/vue-placeholders.js",
    "~/plugins/vue-observe-visibility.client.js",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ["@nuxtjs/style-resources", "@nuxtjs/svg"],

  // Modules: https://go.nuxtjs.dev/config-modules

  modules: ["nuxt-leaflet"],

  // to deploy like SSG (static)
  target: "static",

  /**
   * deploy on Gitlab pages (CI/CD)
   */
  router: {
    base,
  },

  generate: {
    subFolders: false,
    dir: "public",
    fallback: "404.html",
  },

  build: {},
};
